import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

import Modal from 'components/modal';

const ServiceTitanIntegrationHow = () => (
    <Modal title="ServiceTitan Integration: How ordering works" open>
        <Fragment>
            <p>We can automatically send a gift or eTouch every time you complete a job. You can create <strong>Automation Rules</strong> to control what customers receive.</p>
            <p>We'll start you out with a few example rules to give you an idea of how this works.</p>
            <p>Table</p>
            <Link to="/integrations/servicetitan-step-3" className="btn btn-primary"> Continue </Link>
        </Fragment>
    </Modal>
);

export default ServiceTitanIntegrationHow;
