import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

import Modal from 'components/modal';

const ServiceTitanIntegrationEnableKey = () => (
    <Modal title="ServiceTitan Integration: Overview" open dot={ 3 }>
        <Fragment>
            <p>Paste the API key from your ServiceTitan confirmation email:</p>
            <input type="text" placeholder="0000 - 0000 - 0000 - 00 - 0000" />
            <Link to="/rules" className="btn btn-primary"> Enable Integration </Link>
            <Link to="/integrations/servicetitan-step-4" > Request a new API key </Link>
        </Fragment>
    </Modal>
);

export default ServiceTitanIntegrationEnableKey;
