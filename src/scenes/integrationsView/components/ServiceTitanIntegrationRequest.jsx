import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

import Modal from 'components/modal';

const ServiceTitanIntegrationRequest = () => (
    <Modal title="ServiceTitan Integration: Request Connection" open dot={ 1 }>
        <Fragment>
            <p>ServiceTitan needs to enable your To Your Success connection by generating your API key. Once your request is made, it may take up to 24 hours for the connection to become active.</p>
            <p>By clicking the button below, you consent to allow To Your Success to access your ServiceTitan account on your behalf. </p>
            <Link to="/integrations/servicetitan-step-4" className="btn btn-primary"> Request Conection </Link>
        </Fragment>
    </Modal>
);

export default ServiceTitanIntegrationRequest;
