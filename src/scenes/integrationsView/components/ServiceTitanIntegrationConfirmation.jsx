import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

import Modal from 'components/modal';

const ServiceTitanIntegrationConfirmation = () => (
    <Modal title="ServiceTitan Integration: Overview" open dot={ 2 }>
        <Fragment>
            <p className="color-green">Your connection request has been sent.</p>
            <p>Activation might take up to one business day, and you will be notified by email (johndoe@gmail.com) when the connection is enabled.</p>
            <p>Once the connection is enabled, you will be able to set up rules.</p>
            <p>If you need assistance in the meantime, please reach us at 888-793-2429 or info@toyoursuccess.com</p>

            <Link to="/integrations/servicetitan-step-3" className="btn btn-primary"> Resend Request </Link>
            <p>Already have an API key?
                <Link to="/integrations/servicetitan-step-5"> Enter it here</Link>
            </p>
        </Fragment>
    </Modal>
);

export default ServiceTitanIntegrationConfirmation;
