import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'

import Modal from 'components/modal';
import Integration from 'components/integration';
import titanLogo from 'images/logotypes/servicetitan.png';

const ServiceTitanIntegrationOverview = () => (
    <Modal title="ServiceTitan Integration: Overview" open>
        <Fragment>
            <Integration logo={ titanLogo } label="Service Titan" />
            <p>Connecting your ServiceTitan account will enable you to automatically send a gift or eTouch to your customers and imports your data, such as team member names.</p>
            <Link to="/integrations/servicetitan-step-2" className="btn btn-primary"> Get started </Link>
        </Fragment>
    </Modal>
);

export default ServiceTitanIntegrationOverview;
