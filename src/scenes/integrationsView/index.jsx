import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import IntegrationList from 'components/integrationList';

import './style.css';
import ServiceTitanIntegrationHow from './components/ServiceTitanIntegrationHow';
import ServiceTitanIntegrationOverview from './components/ServiceTitanIntegrationOverview';
import ServiceTitanIntegrationRequest from './components/ServiceTitanIntegrationRequest';
import ServiceTitanIntegrationConfirmation from './components/ServiceTitanIntegrationConfirmation';
import ServiceTitanIntegrationEnableKey from './components/ServiceTitanIntegrationEnableKey';

class IntegrationsView extends Component{
    constructor(props) {

        super(props);

        this.state = {
            modalOpen : false
        };

        this.toggleModal = this.toggleModal.bind(this);
    }

    toggleModal() {
        this.setState(prev => ({
            modalOpen : !prev.modalOpen
        }))
    }

    render() {

        console.log(this.props.location);
        console.log(this.props.match);

        return(
            <div className='container'>
                <h1>Current Integrations</h1>
                <h2>Put To Your Success on auto-pilot!</h2>
                <p>By connecting your business software to To Your Success, customer follow-up is easier and more consistent than ever before.</p>
                <p>With our integrations, you can fully automate your customer gifts, surveys and online review requests.</p>
                <p>Looking for peace of mind? Our customizable rules settings ensure you're still in the driver's seat.</p>

                <IntegrationList/>

                <Route path='/integrations/servicetitan-step-1' component={ ServiceTitanIntegrationOverview } exact />
                <Route path='/integrations/servicetitan-step-2' component={ ServiceTitanIntegrationHow } exact />
                <Route path='/integrations/servicetitan-step-3' component={ ServiceTitanIntegrationRequest } exact />
                <Route path='/integrations/servicetitan-step-4' component={ ServiceTitanIntegrationConfirmation } exact />
                <Route path='/integrations/servicetitan-step-5' component={ ServiceTitanIntegrationEnableKey } exact />

            </div>
        );
    }
}

export default IntegrationsView;
