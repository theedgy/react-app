import React from 'react';

import Toggle from 'components/toggle'
import ConditionList from 'components/conditionList'

const IntegrationRulesView = () => (

    <div className='container'>
        <h1>ServiceTitan Rules</h1>
        <h2>Automatic Ordering</h2>
        <p>We can automatically send gifts each time you complete a customer's job. <strong>Automation Rules</strong> control who receives a gift or eTouch. In case a job matches multiple rules, we'll go by the first rule that matches. <strong>Exclusions</strong> are an override to prevent any orders for cetain scenarios.</p>
        <div className='rules-settings'>
            <p className='rules-activation'>Status: OFF (missing API key)  </p><Toggle />
            <p className='rules-maps'>Department Mapping Settings <i>icon</i></p>
        </div>

        <ConditionList />
    </div>
);

export default IntegrationRulesView;
