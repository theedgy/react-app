import React from 'react';

const IconEdit = () => (
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="-293 385 24 24" style="enable-background:new -293 385 24 24;">
        <g id="Bounding_Boxes">
            <g id="ui_x5F_spec_x5F_header_copy_3" />
            <path style="fill:none;" d="M-293,385h24v24h-24V385z"/>
        </g>
        <g id="Sharp">
            <g id="ui_x5F_spec_x5F_header_copy_2" />
            <path style="fill:#797979;" d="M-290,402.3v3.7h3.8l11-11.1l-3.8-3.8L-290,402.3z M-271.6,391.3l-3.8-3.8l-2.5,2.5l3.8,3.8L-271.6,391.3z"/>
        </g>
    </svg>
);

export default IconEdit;
