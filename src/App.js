import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Loading from 'components/loading';
import Navbar from 'components/navbar';

const IntegrationsView = Loadable({
    loader: () => import('scenes/integrationsView'),
    loading: Loading
});

const Rules = Loadable({
    loader: () => import('scenes/integrationRulesView'),
    loading: Loading
});

const Activity = Loadable({
    loader: () => import('scenes/activity'),
    loading: Loading
});

const App = () => (
    <BrowserRouter className='container'>
        <React.Fragment>

            <Navbar />

            <Switch>
                <Route path='/integrations' component={ IntegrationsView } />
                <Route path='/rules' component={ Rules } />
                <Route path='/activity' component={ Activity } />
            </Switch>

        </React.Fragment>
    </BrowserRouter>
);

export default App;
