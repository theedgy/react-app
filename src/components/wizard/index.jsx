import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Wizard = ({ children }) => (
    <div className='modal-wizard'> { children } </div>
);

Wizard.propTypes = {
    children: PropTypes.element.isRequired
};

export default Wizard;
