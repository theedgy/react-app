import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { Button } from 'reactstrap';

import './style.css';
import logo from 'images/logotypes/main-logo.jpg';

// User data
const user = {
    type: 'Platinum member',
    name: 'Amanda Poling'
};

//Menu data
const menu = [
    {
        name : 'Dashboard',
        url : '#'
    },
    {
        name : 'Orders',
        url : '#'
    },
    {
        name : 'Reports',
        url : '#'
    },
    {
        name : 'Invoices',
        url : '#'
    },
    {
        name : 'Enclosures',
        url : '#'
    },
    {
        name : 'Logo products',
        url : '#'
    },
    {
        name : 'Automation',
        url : '#',
        subnav : [
            {
                name : 'Integrations',
                url : '/integrations'
            },
            {
                name : 'Rules',
                url : '/rules'
            },
            {
                name : 'Activity',
                url : '/activity'
            }
        ]
    },
    {
        name : 'Settings',
        url : '#'
    }
];

class Navbar extends Component{
    constructor(props) {
        super(props);

        this.state = {
            currentMenuItem : 6
        }
    }

    render() {
        return(
            <header className='header-main'>
                <div className='header-top'>
                    <div className='container'>
                        <span className='welcome'>Welcome <span className='user-type'>{ user.type }</span></span>
                        <Button color='orange' className='secondary user-link'>{ user.name }</Button>
                    </div>
                </div>

                <div className='header-middle'>
                    <div className='container'>
                        <img src={ logo } alt='logo'/>

                        <nav className='main-nav'>
                            <ul>
                                {menu.map(menuElement => (
                                    <li key={ menuElement.name }>
                                        <Link to={ menuElement.url }>{ menuElement.name }</Link>
                                    </li>
                                ))}
                            </ul>
                        </nav>

                    </div>
                </div>

                <div className='header-bottom'>
                    <div className='container'>

                        <span className='current-page'>{ menu[ this.state.currentMenuItem ].name }</span>

                        <nav className='sub-nav'>
                            <ul>
                                {menu[ this.state.currentMenuItem ].subnav.map(menuElement => (
                                    <li key={ menuElement.name }>
                                        <NavLink to={ menuElement.url }>
                                            { menuElement.name }
                                        </NavLink>
                                    </li>
                                ))}
                            </ul>
                        </nav>

                    </div>
                </div>
            </header>
        );
    }
}

export default Navbar;
