import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Integration = ({ status, logo, label }) => (
    <div className={ `integration-logo ${ status }` } >

        <figure className='integration-logo__image'>
            <img src={ logo } alt={ label }/>
        </figure>

        <span className='integration-logo__label'>{ label }</span>
    </div>
);

Integration.propTypes = {
    status: PropTypes.string,
    logo: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
};

Integration.defaultProps = {
    status: ''
};

export default Integration;
