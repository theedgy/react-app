import React from 'react';
import Integration from 'components/integration';

import { Link } from 'react-router-dom'

import './style.css';

// Data for integrations elements
import coolfrontLogo from 'images/logotypes/coolfront.png';
import fieldedgeLogo from 'images/logotypes/desco.png';
import titanLogo from 'images/logotypes/servicetitan.png';
import zapierLogo from 'images/logotypes/zapier.png';

const integrationElements = [
    {
        id: 'coolfront',
        label: 'Coolfront',
        logo: coolfrontLogo,
        status: 'disabled'
    },
    {
        id: 'fieldedge',
        label: 'FieldEdge',
        logo: fieldedgeLogo,
        status: 'disabled'
    },
    {
        id: 'servicetitan',
        label: 'Service Titan',
        logo: titanLogo,
        status: 'active'
    },
    {
        id: 'zapier',
        label: 'Zapier',
        logo: zapierLogo,
        status: 'warning'
    }
];

const IntegrationList = () => (
    <div className='integration-links'>
        { integrationElements.map(el => (
            <Link key={ el.label } to={ `/integrations/${ el.id }-step-1` }>
                <Integration
                    logo={ el.logo }
                    label={ el.label }
                    status={ el.status }/>
            </Link>
        ))}
        <div className='add-element' />
    </div>
);

export default IntegrationList;
