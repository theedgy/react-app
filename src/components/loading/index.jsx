import React from 'react';

import './style.css';

const Loading = () => (
    <div className='loading-element'>Loading...</div>
);

export default Loading;
