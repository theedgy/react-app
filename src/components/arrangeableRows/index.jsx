import React, { Fragment } from 'react';

import Condition from 'components/condition';
import image from 'images/4.jpg';

//Data condition list
const conditions = [
    {
        item : image,
        settings: [
            'East > HVAC Installation',
            'Thank You Card'
        ],
        when: [
            <p>Order total is less than <strong>$2500</strong>, and</p>,
            <p>Business Unit is <strong>East - HVAC Install</strong></p>
        ],
        active: true
    },
    {
        item : image,
        settings: [
            'East > HVAC Installation',
            'Thank You Card'
        ],
        when: [
            <p>Order total is less than <strong>$2500</strong>, and</p>,
            <p>Business Unit is <strong>East - HVAC Install</strong></p>
        ],
        active: false
    },
    {
        item : image,
        settings: [
            'East > HVAC Installation',
            'Thank You Card'
        ],
        when: [
            <p>Order total is less than <strong>$2500</strong>, and</p>,
            <p>Business Unit is <strong>East - HVAC Install</strong></p>
        ],
        active: false
    }
];

const ArrangeableRows = () => (
    <Fragment>
        <tbody>
            {conditions.map((condition, i )=> (
                <Condition rowNumber={ i+1 } item={ condition.item } settings={ condition.settings } when={ condition.when } isActive={ condition.active } key={ condition.item } />
            ))}
        </tbody>
    </Fragment>
);

export default ArrangeableRows;
