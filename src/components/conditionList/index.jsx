import React from 'react';
import { Table } from 'reactstrap';

import ArrangeableRows from 'components/arrangeableRows';

import './style.css';

const ConditionList = () => (

    <Table>
        <thead>
            <tr>
                <th></th>
                <th>SEND ITEM</th>
                <th>SETTINGS</th>
                <th>WHEN</th>
                <th></th>
                <th>ENABLED</th>
            </tr>
        </thead>

        <ArrangeableRows />
    </Table>
);

export default ConditionList;
