import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Modal, ModalHeader, ModalBody } from 'reactstrap';

import Wizard from 'components/wizard';
import './style.css';

const ModalElement = ({ open, title, className, children, dot }) => (
    <Modal
        isOpen={ open }
        className={ className }>

        <ModalHeader>
            { title }
        </ModalHeader>

        <ModalBody>
            <Wizard>
                <Fragment>
                    { children }
                    <div className="dots" data-dot={ dot }>
                        <span className={ dot === 1 ? 'active' : '' } />
                        <span className={ dot === 2 ? 'active' : '' } />
                        <span className={ dot === 3 ? 'active' : '' } />
                    </div>
                </Fragment>
            </Wizard>
        </ModalBody>

    </Modal>
);

ModalElement.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string.isRequired,
    className: PropTypes.string,
    children: PropTypes.element.isRequired,
    dot: PropTypes.number
};

ModalElement.defaultProps = {
    open: false,
    className: '',
    dot: 0
};

export default ModalElement;
