import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Toggle = ({ active }) => (
    <div className={ `toggler ${ active ? 'active' : 'inactive' }` } />
);

Toggle.propTypes = {
    active: PropTypes.bool
};

Toggle.defaultProps = {
    active: false
};

export default Toggle;
