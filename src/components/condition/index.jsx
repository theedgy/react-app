import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// import IconEdit from '../../images/icons/icon-edit';
import Toggle from 'components/toggle'

const Condition = ({ rowNumber, item, settings, when, isActive }) => (
    <Fragment>
        <tr>
            <th scope='row'>{ rowNumber }</th>
            <td><img src={ item } width='80' height='80' alt='item'/></td>
            <td>
                <ul>
                    {settings.map(setting => (
                        <li key={ setting }>{ setting }</li>
                    )) }
                </ul>
            </td>
            <td>
                <ul>
                    {when.map(setting => (
                        <li key={ setting.props.children }>{ setting }</li>
                    )) }
                </ul>
            </td>
            <td>
            </td>
            <td><Toggle active={ isActive }/></td>
        </tr>
    </Fragment>
);

Condition.propTypes = {
    rowNumber: PropTypes.number.isRequired,
    item: PropTypes.string.isRequired,
    settings: PropTypes.array.isRequired,
    when: PropTypes.array.isRequired,
    isActive : PropTypes.bool
};

Condition.defaultProps = {
    isActive: false
};

export default Condition;
