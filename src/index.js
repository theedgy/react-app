import React from 'react';
import ReactDOM from 'react-dom';

import App from 'App';

import './index.css';
import 'normalize.css';

// Uncomment this if you want to use the Service Worker.
// import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<App />,document.getElementById("root"));

// Uncomment this if you want to use the Service Worker.
// registerServiceWorker();
